+++
title = "About"
description = "About the LinuxMasterRace"
aliases = ["about-lmr"]
author = "LMR Authors"
+++

A subset of the Glorious PC Master Race, the Linux Master Race promotes the glorious operating systems that run using the free and open source Linux kernel. Linux differs from the disgustingly proprietary operating systems like Windows and Mac OS by being open and freely influenced by anyone with the necessary desire and abilities.

# Why use Linux?

* Less malware than OSX and Windows.
* It's faster and more efficient than OSX and Windows.
* No bloatware included.
* You can make your desktop look however you want. [Literally](https://reddit.com/r/unixporn/).
* You can update all your computer's software with a single click.
* Runs on pretty much anything, no matter how old it is.
* You can see every line of code it uses, so you know there's no NSA backdoor. [They tried (and failed) to get a backdoor inserted into Linux](https://www.theregister.com/2013/09/19/linux_backdoor_intrigue/). There are also no keyloggers built-in, unlike in Windows
* It costs exactly $0.
* According to Gabe Newell, it's the future of PC gaming  
  (as is [SteamOS](https://store.steampowered.com/steamos/buildyourown)).
* By using it, you ascend even higher into the [/r/PCMasterRace](https://reddit.com/r/pcmasterrace/)
* [Read more](http://www.whylinuxisbetter.net/)
* [Also check our wiki (WIP)](https://reddit.com/r/linuxmasterrace/wiki/index)

<sup>(The full text can be found in the sidebar of the subreddit.)</sup>
