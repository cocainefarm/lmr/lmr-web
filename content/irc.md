+++
title = "IRC"
description = "Internet Relay Chat"
aliases = ["irc"]
author = "LMR Authors"
+++

LMR uses **Internet Relay Chat** ([IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat)) as live
communication/chat.

## Content

- [Information](#information)
- [Rules](#rules)
- [Advice](#advice)
- [Approved Tools](#approved-tools)
- [Dashboard](#dashboard)
- [Bots](#bots)

## Information

- **Network**: [irc.snoonet.org](https://snoonet.org/)
- **Channel**: #linuxmasterrace
- **Web client**: http://webchat.snoonet.org/linuxmasterrace
- **Regular client**: [irc://irc.snoonet.org/linuxmasterrace](irc://irc.snoonet.org/linuxmasterrace)
- **Matrix**: #linuxmasterrace:matrix.org

## Rules

Everyone who is active within the channel must follow certain rules in order for everyone to have an
enjoyable experience.

### Main Rules

- Don't be a dick
- Don't spam
- Ops have the right to kick/ban anyone no matter what if they feel like it.


## Advice

Newcomers should stay in the channel for a while when they join and say hello. There are
downtimes when not many people are online or most of the "online" people are actually afk or idle.
This may result in very delayed responses.

Fortunately running IRC is cheap so many people leave their IRC running constantly
and/or have a setup that allows them to attach to a running IRC session hosted somewhere else,
preserving the logs while they are away. This is an imporant thing to know about IRC:
**Messages sent during the time you are disconnected do not show up when you reconnect.**

It's frustrating/disappointing when a newcomer joins the channel, says hi, and
disconnects after a few minutes because they received no response. There is no way of responding to
them once they leave the channel, since they won't see your messages after they rejoin. (Except for using
[gonzobot's](#bots) `.tell` command.)

## Approved Tools

IRC only allows sending plain text messages. (Some formatting is possible by using escape codes.)
How to send files or code snippets may be an unexpected hurdle when it comes to it. Certain tools
made by channel members make it easier or possible to do this, these are listed here:

- [**pastor**](https://c-v.sh/): Upload files or text pastes/code snippets
- [**thecum.zone**](https://thecum.zone): Upload files

Do not try to paste multi-line code snippets into the chat, as it will trigger
flood protection at 5 concurrent lines. Upload multi-line entries in a pastebin like
pastor and then send the link to it.

## Dashboard

There is a dashboard showing live information and messages from the channel, made by some members:
[lmrdashboard](https://dash.linuxmasterrace.org). This makes it possible to lurk without even
joining the channel. Some leaderboards are also tracked there.

## Bots

There are a few bots connected to the channel which are either useful, entertaining, or both. (Or
maybe useless after all.)

The following (incomplete) list shows the current (2021-07-03) bots and their uses:

- [**gonzobot**](https://snoonet.org/gonzobot/): Main bot of snoonet
- **aboftybot**: Various commands
- **muh_bot**: Various commands
- **nuh_bot**: Subwatch replacement
- **`\__{^-_-^}`**: Cat-themed commands
- **zinixbot**: Dead most of the time

New bots are generally not discouraged, contrary to what any legacy rules mention. Their development
process should occur in a different channel, but once they are in a production state they are
welcome to join the channel, as long as they're not spamming or doing similar malicious things.
