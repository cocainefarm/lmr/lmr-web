+++
title = "LinuxMasterRace"
disableTitleSeparator = true
+++

Welcome to the home of the LinuxMasterRace.

Read more about us in the About section linked above.  
Join the the subreddit, the IRC channel, or see some live information in the dashboard.

* [Rules](/rules/)
* [More links](/links/)

## Ducc

{{< youtube Y5NTgZA-xWE >}}
<br>
