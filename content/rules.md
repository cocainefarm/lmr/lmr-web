---
title: 'Rules'
---

* [Subreddit rules](https://reddit.com/r/linuxmasterrace/about/rules/)
* [IRC channel rules](https://github.com/linuxmasterrace/IRC-Rules)
